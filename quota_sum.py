#!/usr/bin/env python3
from datetime import datetime
import subprocess
import sys
import shutil

cmd = [
    '/usr/sbin/repquota',
    '-a',
]

try:
    out = subprocess.check_output(cmd).decode("utf-8").rstrip()
except subprocess.CalledProcessError as e:
    out = e.output.decode("utf-8").rstrip()

lines = out.rstrip().splitlines()

timestamp = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
used = 0
assigned_soft = 0
assigned_hard = 0
files = 0
users = 0


if len(lines) > 5:
    for line in lines[5:]:
        fields = line.split()
        users += 1
        used += int(fields[2])
        assigned_soft += int(fields[3])
        assigned_hard += int(fields[4])

        # When graceperiod is set the table slides one field to the right
        if len(fields) == 8:
            files += int(fields[5])
        else:
            files += int(fields[6])

else:
    print('Sorry, something went wrong while reading quota from system.', file=sys.stderr)
    sys.exit(1)

# repquota reports kilobytes so we have to convert it to terabytes
used /= 1024**3
assigned_soft /= 1024**3
assigned_hard /= 1024**3

# get totals and convert them to terabytes
total, used, free = shutil.disk_usage("/home")
total /= 1024**3
used /= 1024**3
free /= 1024**3

print(','.join(str(x) for x in [
    timestamp, round(used, 1), round(assigned_soft, 1), round(assigned_hard, 1), files, users, round(total, 1), round(used, 1)
]))
