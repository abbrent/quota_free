#!/usr/bin/env python3
import os
import requests

from dmp_upload import DmpUpload

__author__ = 'abbrent'

dir = os.path.dirname(__file__)
source_url = 'https://files.ufz.de/~big-data-reporting/quota_sum.csv'
device_id = open(dir + '/.deviceId').read().strip()
prefix = 'sftp-big-data-moni-'
suffix = 'csv'

data = requests.get(source_url).text
dmp = DmpUpload(device_id)
dmp.upload_data(data, filename_prefix=prefix, filename_suffix=suffix)
