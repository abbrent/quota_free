#!/usr/bin/env python3
import getopt

import subprocess
import sys

__author__ = 'abbrent'

def sizeof_fmt(num, suffix='B'):
    for unit in ['Ki','Mi','Gi','Ti','Pi','Ei','Zi']:
        if abs(num) < 1024.0:
            return "%3.1f%s%s" % (num, unit, suffix)
        num /= 1024.0
    return "%.1f%s%s" % (num, 'Yi', suffix)


# quota -wp -f /home/ --show-mntpoint --hide-device

cmd = [
    'quota',
    '-wp',
    # '-f',
    # '/home/',
    '--show-mntpoint',
    # '--hide-device',
]

try:
    out = subprocess.check_output(cmd).decode("utf-8").rstrip()
except subprocess.CalledProcessError as e:
    out = e.output.decode("utf-8").rstrip()

# out = """Disk quotas for user testquota (uid 1008):
#        Filesystem  blocks   quota   limit   grace   files   quota   limit   grace
#             /dev/mapper/vg0-lvol0 /home  103452* 102400  112640 1438349494       9       0       0       0
#            /dev/mapper/vg0-lvol0-sdfasd /home/sfsdf  50000* 102400  112640 1438349494       9       0       0       0
#            /dev/sda /      36  16106127360 17716740096       0      10       0       0       0"""

header = [
    "Filesystem",
    "Size",
    "Used",
    "Available",
    "Use%",
    "Mounted on"
]

table_format = "{:<32} {:<16} {:<16} {:<16} {:<5} {:<12}"
lines = out.splitlines()
human_readable = False

# Check arguments
try:
    argv = sys.argv[1:]
    opts, args = getopt.getopt(argv, 'h', ['human-readable'])
except getopt.GetoptError:
    sys.exit(2)

for opt, arg in opts:
    if opt in ('-h', '--human-readable'):
        human_readable = True

if len(lines) > 2:
    print(table_format.format(*header))
    for line in lines[2:]:
        fields = line.split()
        filesystem = fields[0]
        mountpoint = fields[1]
        used = int(fields[2].replace('*', ''))
        quota = int(fields[3])
        avail = quota - used
        if (avail < 0): avail = 0
        use_perc = int(round(used / quota * 100))
        if (use_perc > 100): use_perc = 100

        if human_readable:
            quota = sizeof_fmt(quota)
            used = sizeof_fmt(used)
            avail = sizeof_fmt(avail)

        ret = [filesystem, quota, used, avail, use_perc, mountpoint]
        print(table_format.format(*ret))
else:
    print('No quota available. Use `df` instead.', file=sys.stderr)
    sys.exit(1)
