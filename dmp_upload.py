from _datetime import datetime
import requests
from requests import Response

__author__ = 'abbrent'


class DmpUpload:

    # response: Response
    DEFAULT_URL = 'https://logger-worker.intranet.ufz.de/gateway/upload/device/'

    def __init__(self, device_id, base_url=DEFAULT_URL):
        self.device_id = device_id
        self.base_url = base_url
        self.url = base_url + device_id

    def upload_file(self, file):
        self.response = requests.post(self.url, files={
            'file': file
        })

        self.handle_response()

        return self.response.text

    def upload_data(self, data, filename_prefix='', filename_suffix='', filename=''):
        if filename == '':
            filename = filename_prefix + datetime.now().strftime("%Y-%m-%d-%H%M%S") + '.' + filename_suffix

        self.response = requests.post(self.url, files={
            'file': (
                filename, data,
            )
        })

        self.handle_response()

        return self.response.text

    def handle_response(self):
        if self.response.status_code != requests.codes.ok:
            raise ConnectionError('Upload not successful: "%s"' % self.response.text)
